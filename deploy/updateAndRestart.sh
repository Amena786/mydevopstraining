#!/bin/bash

# any future command that fails will exit the script
set -e

cd /home/ec2-user

# Delete the old repo
sudo rm -rf /home/ec2-user/node-postgres-todo

# clone the repo again
git clone https://gitlab.com/daveu7/node-postgres-todo.git

#install npm packages
cd /home/ec2-user/node-postgres-todo
echo "Running npm install"
npm install

#Restart the node server
echo "Starting the newly cloned node server"

sudo su
DATABASE_URL=postgres://AmenaDBInstance:Allah786database-2.cgspqlfmy3v6.eu-west-2.rds.amazonaws.com:5432/todo PORT=80 npm start